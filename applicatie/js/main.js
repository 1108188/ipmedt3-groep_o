window.onload = () => {
  const verder_knop = document.getElementById('js--verder_knop');
  const uiText = document.getElementById('js--uiText');
  const bril = document.getElementById('js--bril');
  const labjas = document.getElementById('js--labjas');
  const brilGlas = document.getElementById('js--brilGlas');
  const camera = document.getElementById('js--camera');
  const uiBox = document.getElementById('js--uiBox');
  const deur = document.getElementById('js--deur');
  const deurdraai = document.getElementById('js--deurdraai');
  const tafeltje = document.getElementById('js--tafeltje');
  const toDeur = document.getElementById('js--toDeur');
  const reageerbuis = document.getElementById('js--reageerbuis');
  const pickups = document.getElementsByClassName('js--pickup');
  const scene = document.getElementById('js--scene');
  const places = document.getElementsByClassName("js--place");
  const la = document.getElementById("js--la");
  const closeButton = document.getElementById("js--closeButton");
  const jerrycanDop = document.getElementsByClassName('js--jerrycanDop');
  const toKast = document.getElementById('js--toKast');
  const toTable = document.getElementById('js--toTable');
  const cameraLocation = document.getElementById('js--cameraLocation');
  const flowChart = document.getElementById('js--flowChart');
  const afvalVat1 = document.getElementById('js--afvalVat1');
  const afvalVat2 = document.getElementById('js--afvalVat2');
  const afvalVat3 = document.getElementById('js--afvalVat3');
  const afvalVat4 = document.getElementById('js--afvalVat4');
  const afvalVat5 = document.getElementById('js--afvalVat5');
  const afvalVat6 = document.getElementById('js--afvalVat6');
  const chemicalInfo = document.getElementById('js--chemicalInfo');
  const chemicalName = document.getElementById('js--chemicalName');
  const chemicalPh = document.getElementById('js--chemicalPh');
  const emmer = document.getElementById('js--emmer');
  const plantpot = document.getElementById('js--plantpot');
  const weggooibuisje1 = document.getElementById('js--weggooibuisje1');
  const weggooibuisje2 = document.getElementById('js--weggooibuisje2');
  const weggooibuisje3 = document.getElementById('js--weggooibuisje3');
  const schoonmaakbutton = document.getElementById('js--schoonmaak-button');
  const reageerbuisrek = document.getElementById('js--reageerbuisrekje');
  const driepoot = document.getElementById('js--driepoot');
  const schoonmaakdoek = document.getElementById('js--schoonmaakdoekje');
  const closeText = document.getElementById('js--closeText');
  var buisNum = 0;
  const stoffen = [
    {
        "stofnr": 2,
        "naamstof": "BariumHydroxide",
        "soort": "Base",
        "pH": 13,
        "VerbindingSoort": "Anorganisch",
        "formule": "Ba(OH)2",
        "vat": 2

    },
    {
        "stofnr": 3,
        "naamstof": "Calciumhydroxide",
        "soort": "Base",
        "pH": 10,
        "VerbindingSoort": "Anorganisch",
        "formule": "Ca(OH)2",
        "vat": 2
    },
    {
        "stofnr": 4,
        "naamstof": "KaliumHydroxide",
        "soort": "Base",
        "pH": 9,
        "VerbindingSoort": "Anorganisch",
        "formule": "KOH",
        "vat": 2

},
    {
        "stofnr": 4,
        "naamstof": "NatriumHydroxide",
        "soort": "Base",
        "pH": 11,
        "VerbindingSoort": "Anorganisch",
        "formule": "NaOH",
        "vat": 2

}]


const stoffen2 =  [{
        "stofnr": 5,
        "naamstof": "Boorzuur",
        "soort": "Zuur",
        "pH": 3,
        "VerbindingSoort": "Anorganisch",
        "formule": "H3BO3",
        "vat": 1
},
{
        "stofnr": 6,
        "naamstof": "Fosforzuur",
        "soort": "Zuur",
        "pH": 2,
        "VerbindingSoort": "Anorganisch",
        "formule": "H3PO4",
        "vat": 1
},
{
        "stofnr": 6,
        "naamstof": "Salpeterzuur",
        "soort": "Zuur",
        "pH": 1,
        "VerbindingSoort": "Anorganisch",
        "formule": "HNO3",
        "vat": 1
},
{
        "stofnr": 7,
        "naamstof": "Waterstofbromide",
        "soort": "Zuur",
        "pH": 2,
        "VerbindingSoort": "Anorganisch",
        "formule": "HBr",
        "vat": 1
},
{
        "stofnr": 8,
        "naamstof": "Waterstofperchloraat",
        "soort": "Zuur",
        "pH": 0,
        "VerbindingSoort": "Anorganisch",
        "formule": "HCIO4",
        "vat": 1
},
{
        "stofnr": 9,
        "naamstof": "Zoutzuur",
        "soort": "Zuur",
        "pH": 0,
        "VerbindingSoort": "Anorganisch",
        "formule": "HCl",
        "vat": 1
},
{
        "stofnr": 10,
        "naamstof": "Zwavelzuur",
        "soort": "Zuur",
        "pH": 0,
        "VerbindingSoort": "Anorganisch",
        "formule": "H2SO4",
        "vat": 1
},
{
        "stofnr": 11,
        "naamstof": "Chroom(VI)oxide",
        "soort": "Metalen",
        "pH": 1,
        "VerbindingSoort": "Anorganisch",
        "formule": "CrO3",
        "vat": 1
}
]

const stoffen3 = [
  {
          "stofnr": 12,
          "naamstof": "Mangaan(II)Chloride",
          "soort": "Zwaar Metaal",
          "pH": 7,
          "VerbindingSoort": "",
          "formule": "",
          "vat": 5
  },
  {
          "stofnr": 13,
          "naamstof": "ZinkSulfaat",
          "soort": "Zwaar Metaal",
          "pH": 7,
          "VerbindingSoort": "",
          "formule": "",
          "vat": 5
  },
  {
          "stofnr": 14,
          "naamstof": "Bismut(III)Oxide",
          "soort": "Zwaar Metaal",
          "pH": 7,
          "VerbindingSoort": "",
          "formule": "",
          "vat": 5
  },
  {
          "stofnr": 15,
          "naamstof": "Koper(II)Sulfaat",
          "soort": "Zwaar Metaal",
          "pH": 7,
          "VerbindingSoort": "",
          "formule": "",
          "vat": 5
  }

]
  const textToSpeech = (text) => {
    responsiveVoice.speak(text.getAttribute("value"), "Dutch Male");
  }

  const numberOfStoffen = 1;
  // array waar random indexes inkomen zodat er geen dubbele zijn
  let randomIndexes = [];
  //for-loop die random nummers genereert voor het aantal wat de variabele numberOfStoffen is
  for(let i = 0; i < numberOfStoffen; i++){
    //maak een random getal aan
    let randomIndex = Math.floor(Math.random()*stoffen.length);
     //kijk of hij in de lijst staat, zoja: blijf opnieuw random nummers genereren  totdat er een nummer niet in de lijst staat
    while(randomIndexes.includes(randomIndex)){
      randomIndex = Math.floor(Math.random()*stoffen.length);
    }
    //zodra hij klaar is met random nummer genereren > stop hem in de array
    randomIndexes.push(randomIndex);
  }

  //for-loop voor alle random-indexes in de array
  for(let i = 0; i < randomIndexes.length; i++){
    //print het random stof uit
  }

  console.log(stoffen[randomIndexes[0]]);

  const numberOfStoffen2 = 1;
  console.log(stoffen2[randomIndexes[0]]);
  console.log(stoffen3[randomIndexes[0]]);


  textToSpeech(uiText);
  let hold = null;
  verder_knop.addEventListener("click", verderGaan = (event) => {
    uiText.setAttribute("value", "Voordat je het lab in mag, moet je natuurlijk eerst je bril opdoen. Deze ligt op de tafel naast je. Om spullen op te pakken moet je er een seconde naar kijken. ");
    textToSpeech(uiText);
    brilGlas.setAttribute("class", "waypoint");
    labjas.setAttribute("class", "waypoint");
    verder_knop.remove();
    brilGlas.addEventListener("click", brilOp = (event) => {
      bril.remove();
      camera.innerHTML -= '<a-entity id="js--labjasAchter" animation="property: position; to: 0 0 5; dur: 2000; delay: 1000"><a-entity id="js--labjas" position="0 -2 -2" gltf-model="#labcoat"></a-entity></a-entity>'
      camera.innerHTML += "<a-entity animation__click='property: scale; easin: easeInCubic; dur: 150; from: 0.1 0.1 0.1; to: 1 1 1; startEvents: click' animation__fusing='property: scale; easing: easeInCubic; dur: 1500; from: 1 1 1; to: 0.1 0.1 0.1; startEvents: fusing' animation__mouseLeave='property: scale; easing: easeInCubic; dur: 500; to: 1 1 1; startEvents: mouseleave' cursor='fuse: true; fuseTimeout: 1500;' material='color: grey; shader: flat;' geometry='primitive: ring; radiusInner: 0.005; radiusOuter: 0.01' position='0 0 -0.5' raycaster='objects: .waypoint'></a-entity>"
      camera.innerHTML += '<a-entity id="js--brilAchter" position="-.1 0 -2" scale=".1 .1 .1" rotation="0 180 0" animation="property: position; to: 0 0 5; dur: 2000; delay: 1000;"><a-entity id="js--brilGlas" position="0 0 0" gltf-model="#glass"></a-entity><a-entity position="0 0 0" gltf-model="#frame"></a-entity></a-entity>'
      const brilAchter = document.getElementById('js--brilAchter');
      setTimeout(changeText = (event) => {
        uiText.setAttribute("value", "Goed gedaan! Nu je labjas nog!");
        textToSpeech(uiText);
        brilAchter.remove();
        labjas.addEventListener('click', jasAan = (event) => {
          labjas.remove();
          camera.innerHTML -= '<a-entity id="js--brilAchter" position="-.1 0 -2" scale=".1 .1 .1" rotation="0 180 0" animation="property: position; to: 0 0 5; dur: 2000; delay: 1000;"><a-entity id="js--brilGlas" position="0 0 0" gltf-model="#glass"></a-entity><a-entity position="0 0 0" gltf-model="#frame"></a-entity></a-entity>'
          camera.innerHTML += "<a-entity animation__click='property: scale; easin: easeInCubic; dur: 150; from: 0.1 0.1 0.1; to: 1 1 1; startEvents: click' animation__fusing='property: scale; easing: easeInCubic; dur: 1500; from: 1 1 1; to: 0.1 0.1 0.1; startEvents: fusing' animation__mouseLeave='property: scale; easing: easeInCubic; dur: 500; to: 1 1 1; startEvents: mouseleave' cursor='fuse: true; fuseTimeout: 1500;' material='color: grey; shader: flat;' geometry='primitive: ring; radiusInner: 0.005; radiusOuter: 0.01' position='0 0 -0.5' raycaster='objects: .waypoint'></a-entity>"
          camera.innerHTML += '<a-entity id="js--labjasAchter" animation="property: position; to: 0 0 5; dur: 2000; delay: 1000"><a-entity id="js--labjas" position="0 -2 -2" gltf-model="#labcoat"></a-entity></a-entity>'

          uiText.setAttribute('value', 'Heel goed! Nu is het tijd voor het echte werk. Je kan bewegen door naar de pijltjes op de grond te kijken. Loop maar naar de deur toe!');
          textToSpeech(uiText);
          toDeur.classList.add("waypoint");
          toDeur.setAttribute("visible", "true");
        })
      }, 3000);
    })

  })

  toDeur.addEventListener("click", naarDeur = (event) => {
    const labjasAchter = document.getElementById('js--labjasAchter');
    labjasAchter.remove();
    deur.setAttribute("animation", "property: rotation; to: 0 0 0; dur: 1000;");
    deur.setAttribute("animation__pos", "property: position; to: 4 1.5 -3.5; dur: 1000;")
    uiBox.setAttribute("position", "-1.5 3 -14");
    tafeltje.remove();
    cameraLocation.setAttribute("animation", "property: position; to: 3 0 -5; dur: 3000; easing: linear; delay: 1000;");
    setTimeout(giveWaypoint = (event) => {
      for (var i = 0; i < places.length; i++) {
        places[i].classList.add("waypoint");
      }
      uiText.setAttribute("value", "Je kan nu naar de tafel lopen om een reageerbuisje uit te kiezen.");
      textToSpeech(uiText);
      cameraLocation.setAttribute("animation", "property: position; to: 0 0 -7; dur: 3000; easing: linear; delay: 500;")
      deur.setAttribute("animation", "property: rotation; to: 0 -90 0; dur: 1000;");
      deur.setAttribute("animation__pos", "property: position; to: 3 1.5 -5; dur: 1000;");
    }, 4000);
  });

  for (let j = 0; j < pickups.length; j++) {
    pickups[j].addEventListener("click", pakOp = (event) => {
      if (hold == null) {
        tempPickup = pickups[j];
        pickups[j].setAttribute("scale", "0.001 0.001 0.001 ");
        if(buisNum == 0){
          uiText.setAttribute("value", "Goed bezig! Je kan met hulp van de flowchart uitvinden waar je de inhoud van het reageerbuisje weg moet gooien.");
          textToSpeech(uiText);
        }
        if (tempPickup.getAttribute("id") == "js--reageerbuis1") {
          camera.innerHTML += '<a-entity id="js--reageerbuisInHand1" gltf-model = "#reageerbuis-glb" animation="property: position; from: 0 0 -2; to: 1 -.9 -1"></a-entity>';
          hold = "reageerbuisInHand1"
          console.log(hold);
          chemicalInfo.setAttribute("visible", "true");
          // afvalvat 2
          chemicalName.setAttribute("value", stoffen2[randomIndexes[0]].naamstof);
          chemicalPh.setAttribute("value", stoffen2[randomIndexes[0]].pH);
        } else if (tempPickup.getAttribute("id") == "js--reageerbuis2") {
          camera.innerHTML += '<a-entity id="js--reageerbuisInHand2" gltf-model = "#reageerbuis-glb" animation="property: position; from: 0 0 -2; to: 1 -.9 -1"></a-entity>';
          hold = "reageerbuisInHand2"
          console.log(hold);
          chemicalInfo.setAttribute("visible", "true");
          // afvalvat 5
          chemicalName.setAttribute("value", stoffen[randomIndexes[0]].naamstof);
          chemicalPh.setAttribute("value", stoffen[randomIndexes[0]].pH);
        } else if (tempPickup.getAttribute("id") == "js--reageerbuis3") {
          camera.innerHTML += '<a-entity id="js--reageerbuisInHand3" gltf-model = "#reageerbuis-glb" animation="property: position; from: 0 0 -2; to: 1 -.9 -1"></a-entity>';
          hold = "reageerbuisInHand3"
          console.log(hold);
          chemicalInfo.setAttribute("visible", "true");
          //afvalvat 3
          chemicalName.setAttribute("value", stoffen3[randomIndexes[0]].naamstof);
          chemicalPh.setAttribute("value", stoffen3[randomIndexes[0]].pH);
          hold.setAttribute("value", stoffen3[randomIndexes[0]].vat);
        }
      }
      for (var i = 0; i < pickups.length; i++) {
        pickups[i].classList.remove("waypoint")
      }
    })
  };

  emmer.addEventListener("click", buisInEmmer = (event) => {
    if (hold == 'reageerbuis1weggegooid') {
      document.getElementById('js--reageerbuisInHand1').remove();
      let animation = document.createAttribute('animation');
      animation.value = 'property: position; easing: linear; dur: 2000; to: 2 1 -1.3'
      weggooibuisje1.setAttribute('visible', 'true');
      weggooibuisje1.setAttribute('animation', animation.value);
      hold = null;
    } else if (hold == 'reageerbuis2weggegooid') {
      document.getElementById('js--reageerbuisInHand2').remove();
      let animation = document.createAttribute('animation');
      animation.value = 'property: position; easing: linear; dur: 2000; to: 1.85 1 -1.5'
      weggooibuisje2.setAttribute('visible', 'true');
      weggooibuisje2.setAttribute('animation', animation.value);
      hold = null;
    } else if (hold == 'reageerbuis3weggegooid') {
      document.getElementById('js--reageerbuisInHand3').remove();
      let animation = document.createAttribute('animation');
      animation.value = 'property: position; easing: linear; dur: 2000; to: 1.8 1 -1.3'
      weggooibuisje3.setAttribute('visible', 'true');
      weggooibuisje3.setAttribute('animation', animation.value);
      hold = null;
    }
    for (var i = 0; i < pickups.length; i++) {
      pickups[i].classList.add("waypoint")
    }
  });

  for (let i = 0; i < places.length; i++) {
    places[i].addEventListener("click",
      function(evt) {
        let att = document.createAttribute("animation");
        att.value = "property: position; easing: linear; dur: 2000; to: " + this.getAttribute("position").x + " 0 " + this.getAttribute("position").z;
        cameraLocation.setAttribute("animation", att.value);
      });
  }

  closeButton.onmouseenter = () => {
    closeButton.setAttribute("color", "yellow");
  }

  toTable.addEventListener("click", enableTable = (event) => {
    la.setAttribute("class", "");
    if (hold == null) {
      for (var i = 0; i < pickups.length; i++) {
        pickups[i].classList.add("waypoint")
      }
    }
    flowChart.setAttribute("class", "waypoint");
  });


  toKast.addEventListener("click", enableKast = (event) => {
    for (var i = 0; i < pickups.length; i++) {
      pickups[i].classList.remove("waypoint")
    }
    flowChart.setAttribute("class", "");
    chemicalInfo.setAttribute("visible", "false");
    la.setAttribute("class", "waypoint");
    for (let i = 0; i < jerrycanDop.length; i++) {
      jerrycanDop[i].addEventListener("click", draaiDop = (event) => {
        if (jerrycanDop[i].getAttribute("id") == "js--afvalVat1" && hold == "reageerbuisInHand1") {
          const reageerbuisInHand1 = document.getElementById('js--reageerbuisInHand1');
          console.log("goed");
          reageerbuisInHand1.setAttribute("animation__rot", "property:rotation; to: -70 0 0; dur: 1000; delay: 2000; easing: linear;");
          reageerbuisInHand1.setAttribute("animation__move", "property: position; to: .5 0 -1; dur: 1000; easing: linear;");
          reageerbuisInHand1.setAttribute("animation__rotBack", "property:rotation; from: -70 0 0; to: 0 0 0; dur: 1000; delay: 3500; easing: linear;");
          reageerbuisInHand1.setAttribute("animation__moveBack", "property: position; from: .5 0 -1; to: 1 -.9 -1; dur:1000; delay: 4500; easing: linear;");
          flowChart.innerHTML += '<a-image src="img/Vat1.png" />';
          buisNum++;
          hold = "reageerbuis1weggegooid";
          callTts();
        } else {
          console.log("fout");
        }
        if (jerrycanDop[i].getAttribute("id") == "js--afvalVat2" && hold == "reageerbuisInHand2") {
          const reageerbuisInHand2 = document.getElementById('js--reageerbuisInHand2');
          console.log("goed");
          reageerbuisInHand2.setAttribute("animation__rot", "property:rotation; to: -70 0 0; dur: 1000; delay: 2000; easing: linear;");
          reageerbuisInHand2.setAttribute("animation__move", "property: position; to: .5 0 -1; dur: 1000; easing: linear;");
          reageerbuisInHand2.setAttribute("animation__rotBack", "property:rotation; from: -70 0 0; to: 0 0 0; dur: 1000; delay: 3500; easing: linear;");
          reageerbuisInHand2.setAttribute("animation__moveBack", "property: position; from: .5 0 -1; to: 1 -.9 -1; dur:1000; delay: 4500; easing: linear;");
          flowChart.innerHTML += '<a-image src="img/Vat2.png" />';
          buisNum++;
          hold = "reageerbuis2weggegooid";
          callTts();
        } else {
          console.log("fout");
        }
        if (jerrycanDop[i].getAttribute("id") == "js--afvalVat3" && hold == null) {
          const reageerbuisInHand3 = document.getElementById('js--reageerbuisInHand3');
          console.log("goed");
          reageerbuisInHand3.setAttribute("animation__rot", "property:rotation; to: -70 0 0; dur: 1000; delay: 2000; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__move", "property: position; to: .5 0 -1; dur: 1000; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__rotBack", "property:rotation; from: -70 0 0; to: 0 0 0; dur: 1000; delay: 3500; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__moveBack", "property: position; from: .5 0 -1; to: 1 -.9 -1; dur:1000; delay: 4500; easing: linear;");
          flowChart.innerHTML += '<a-image src="img/Vat3.png" />';
          buisNum++;
          hold = "reageerbuis3weggegooid";
          callTts();
        }
        if (jerrycanDop[i].getAttribute("id") == "js--afvalVat4" && hold == null) {
          const reageerbuisInHand3 = document.getElementById('js--reageerbuisInHand3');
          console.log("goed");
          reageerbuisInHand3.setAttribute("animation__rot", "property:rotation; to: -70 0 0; dur: 1000; delay: 2000; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__move", "property: position; to: .5 0 -1; dur: 1000; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__rotBack", "property:rotation; from: -70 0 0; to: 0 0 0; dur: 1000; delay: 3500; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__moveBack", "property: position; from: .5 0 -1; to: 1 -.9 -1; dur:1000; delay: 4500; easing: linear;");
          flowChart.innerHTML += '<a-image src="img/Vat4.png" />';
          buisNum++;
          hold = "reageerbuis3weggegooid";
          callTts();
        }
        if (jerrycanDop[i].getAttribute("id") == "js--afvalVat5" && hold == "reageerbuisInHand3") {
          const reageerbuisInHand3 = document.getElementById('js--reageerbuisInHand3');
          console.log("goed");
          reageerbuisInHand3.setAttribute("animation__rot", "property:rotation; to: -70 0 0; dur: 1000; delay: 2000; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__move", "property: position; to: .5 0 -1; dur: 1000; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__rotBack", "property:rotation; from: -70 0 0; to: 0 0 0; dur: 1000; delay: 3500; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__moveBack", "property: position; from: .5 0 -1; to: 1 -.9 -1; dur:1000; delay: 4500; easing: linear;");
          flowChart.innerHTML += '<a-image src="img/Vat5.png" />';
          buisNum++;
          hold = "reageerbuis3weggegooid";
          callTts();
        }
        if (jerrycanDop[i].getAttribute("id") == "js--afvalVat6" && hold == null) {
          const reageerbuisInHand3 = document.getElementById('js--reageerbuisInHand3');
          console.log("goed");
          reageerbuisInHand3.setAttribute("animation__rot", "property:rotation; to: -70 0 0; dur: 1000; delay: 2000; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__move", "property: position; to: .5 0 -1; dur: 1000; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__rotBack", "property:rotation; from: -70 0 0; to: 0 0 0; dur: 1000; delay: 3500; easing: linear;");
          reageerbuisInHand3.setAttribute("animation__moveBack", "property: position; from: .5 0 -1; to: 1 -.9 -1; dur:1000; delay: 4500; easing: linear;");
          flowChart.innerHTML += '<a-image src="img/Vat6.png" />';
          buisNum++;
          hold = "reageerbuis3weggegooid";
          callTts();
        }
        console.log(jerrycanDop[i].getAttribute("id"));
        console.log(buisNum);
        jerrycanDop[i].setAttribute("animation", "property: rotation; to: 20 0 0; dur: 2000; easing: linear;");
        jerrycanDop[i].setAttribute("animation__pos", "property: position; to: 0 2 1; dur: 2000; easing: linear;");
        jerrycanDop[i].setAttribute("animation__posBack", "property: position; from: 0 2 1; to: 0 0 0; dur: 2000; delay: 4500; easing: linear;");
        jerrycanDop[i].setAttribute("animation__back", "property: rotation; from: 20 0 0; to: 0 0 0; dur: 2000; delay: 4500; easing: linear;");
        setTimeout(reset = (event) => {
          jerrycanDop[i].removeAttribute("animation");
          jerrycanDop[i].removeAttribute("animation__back");
          jerrycanDop[i].removeAttribute("animation__posBack");
          jerrycanDop[i].removeAttribute("animation__pos");
        }, 6500)
      })
    }
  });

  flowChart.addEventListener("click", showFlowChart = (event) => {
    flowChart.setAttribute("class", "");
    flowChart.setAttribute("animation__rot", "property: rotation; to: 0 180 0; dur:1000;");
    flowChart.setAttribute("animation__pos", "property: position; to: 0 4.12 -1.9; dur:1000;");
    flowChart.setAttribute("animation__rotBack", "property: rotation; from: 0 180 0; to: -90 180 0; dur: 1000; delay: 5000;");
    flowChart.setAttribute("animation__posBack", "property: position; from: 0 4.12 -1.9; to: 2 3.12 -.9; dur: 1000; delay: 5000;");
    setTimeout(reset = (event) =>{
      flowChart.setAttribute("class", "waypoint");
      flowChart.removeAttribute("animation__rot");
      flowChart.removeAttribute("animation__pos");
      flowChart.removeAttribute("animation__rotBack");
      flowChart.removeAttribute("animation__posBack");
    }, 6000);
  })

  la.addEventListener("click", openLa = (event) => {
    la.setAttribute("animation", "property: position; to: 0 0 -2");
    for (let i = 0; i < jerrycanDop.length; i++) {
      jerrycanDop[i].classList.add("waypoint");
    }
    closeButton.setAttribute("color", "red");
    closeButton.setAttribute("class", "waypoint")
    la.setAttribute("class", "");
    closeText.setAttribute("visible", "true");
  });

  closeButton.addEventListener("click", closeLa = (event) => {
    la.setAttribute("animation", "property: position; to: 0 0 0");
    closeButton.setAttribute("color", "white");
    for (let i = 0; i < jerrycanDop.length; i++) {
      jerrycanDop[i].classList.remove("waypoint")
    }
    closeButton.setAttribute("class", "");
    la.setAttribute("class", "waypoint");
    closeText.setAttribute("visible", "false");
  });

  closeButton.addEventListener("click", closeLa = (event) => {
    la.setAttribute("animation", "property: position; to: 0 0 0");
    closeButton.setAttribute("color", "white");
    for (let i = 0; i < jerrycanDop.length; i++) {
      jerrycanDop[i].classList.remove("waypoint")
    }
    closeButton.setAttribute("class", "");
    la.setAttribute("class", "waypoint")
  });

  // functie om schoon te maken
  schoonmaakbutton.addEventListener('click', schoonmaken = (event) => {
    console.log("Tijd om schoon te maken!");
    schoonmaakdoek.setAttribute('visible', 'true');
    schoonmaakdoek.setAttribute('position', '-3.3 3.12 -1.8');

    // create different animations
    let animation = document.createAttribute('animation');
    animation.reageerbuisrek = 'property: position; easing: linear; dur: 2000; to: 0 3.6 0';
    animation.flowchart = 'property: position; easing: linear; dur: 2000; to: 2 3.52 -.9';
    animation.driepoot = 'property: position; easing: linear; dur: 2000; to: 2 3.4 .5';
    animation.plantpot = 'property: position; easing: linear; dur: 2000; to: -1.5 3.5 0';

    // add animations to objects
    reageerbuisrek.setAttribute('animation', animation.reageerbuisrek);
    flowChart.setAttribute('animation', animation.flowchart);
    driepoot.setAttribute('animation', animation.driepoot);
    plantpot.setAttribute('animation', animation.plantpot);

    // clean animations
    let animation_clean = document.createAttribute('animation');
    // original position: -3.3 3.12 -1.8
    animation_clean.naarLinks = 'property: position; easing: linear; dur: 2000; to: 1.8 3.12 -1.8';
    animation_clean.naarAchteren = 'property: position; easing: linear; dur: 2000; to: 1.8 3.12 -.6';
    animation_clean.naarRechts = 'property: position; easing: linear; dur: 2000; to: -3.3 3.12 -.6';
    animation_clean.naarAchterenTwee = 'property: position; easing: linear; dur: 2000; to: -3.3 3.12 0.25';
    animation_clean.naarLinksTwee = 'property: position; easing: linear; dur: 2000; to: 1.8 3.12 0.25';

    // add animations to clean doekje
    schoonmaakdoek.setAttribute('animation', animation_clean.naarLinks);
    setTimeout(function () {
      schoonmaakdoek.setAttribute('animation', animation_clean.naarAchteren);
      setTimeout(function () {
        schoonmaakdoek.setAttribute('animation', animation_clean.naarRechts);
        schoonmaakbutton.setAttribute('visible', 'false');
        setTimeout(function () {
          schoonmaakdoek.setAttribute('animation', animation_clean.naarAchterenTwee);
          setTimeout(function () {
            schoonmaakdoek.setAttribute('animation', animation_clean.naarLinksTwee);
            setTimeout(function () {
              schoonmaakdoek.setAttribute('visible', 'false');
            }, 2000);
          }, 2000);
        }, 2000);
      }, 2000);
    }, 2000);

    // add delay to objects before they go back so there is time to clean
    setTimeout(function () {
      // create different animations to go back to original place
      let animation_back = document.createAttribute('animation');
      animation_back.reageerbuisrek = 'property: position; easing: linear; dur: 2000; to: 0 3.2 0';
      animation_back.flowchart = 'property: position; easing: linear; dur: 2000; to: 2 3.12 -.9';
      animation_back.driepoot = 'property: position; easing: linear; dur: 2000; to: 2 3 .5';
      animation_back.plantpot = 'property: position; easing: linear; dur: 2000; to: -1.5 3.1 0';

      // add animations to objects
      reageerbuisrek.setAttribute('animation', animation_back.reageerbuisrek);
      flowChart.setAttribute('animation', animation_back.flowchart);
      driepoot.setAttribute('animation', animation_back.driepoot);
      plantpot.setAttribute('animation', animation_back.plantpot);
      setTimeout(function () {
        uiText.setAttribute("value", "Al het afval is gescheiden en de tafel is weer schoon voor het volgende gebruik. Dit is het einde van de VR beleving.");
        textToSpeech(uiText);
      }, 2000);
    }, 10000);
  });

  const callTts = () => {
    if(buisNum == 1){
      uiText.setAttribute("value", "Dit is het correcte vat. Goed gedaan! Je kan je buisje in de emmer onder de tafel gooien.");
      textToSpeech(uiText);
    }
    if(buisNum == 2){
      uiText.setAttribute("value", "Dit is het correcte vat. Goed gedaan!");
      textToSpeech(uiText);
    }
    if(buisNum == 3){
      uiText.setAttribute("value", "Dit is het correcte vat. Goed gedaan! Je hebt alle stoffen op de goede plek weg gegooid.");
      textToSpeech(uiText);
      chemicalInfo.setAttribute('visible', 'false');
      setTimeout(function () {
        schoonmaakbutton.setAttribute('visible', 'true');
        uiText.setAttribute("value", "Nadat je de la dicht gedaan hebt en je het buisje in de emmer onder tafel hebt gegooid, is het tijd om de tafel schoon te maken. Druk op de schoonmaak knop rechts van de tafel.");
        textToSpeech(uiText);
      }, 8000);
    }
  }
}
