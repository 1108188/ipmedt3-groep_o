window.onload = () => {
  const reageerbuis = document.getElementById('js--reageerbuis');
  const pickups = document.getElementsByClassName('js--pickup');
  const camera = document.getElementById('js--camera');
  const box = document.getElementById('js--box');
  const scene = document.getElementById('js--scene');
  const places = document.getElementsByClassName("js--place");
  const la = document.getElementById("js--la");
  const closeButton = document.getElementById("js--closeButton");

  let hold = null;

  function addListeners() {
    for (let i = 0; i < pickups.length; i++) {
      pickups[i].addEventListener('click', pakOp = (event) => {
        if (hold == null) {
          camera.innerHTML += '<a-entity id="js--hold" obj-model="obj: #reageerbuis-obj"; animation="property: position; from: 1 -2 -1; to: 1 -1 -1; dur: 500;"></a-entity>';
          reageerbuis.remove();
          hold = "reageerbuis";
        }
      })
    }
  }

  box.addEventListener('click', keerReageerbuisOm = (event) => {
    if (hold == "reageerbuis") {
      let reageerbuisje = document.createElement('a-entity');

<<<<<<< HEAD
<<<<<<< HEAD
      // reageerbuis.setAttribute('class', 'js--pickup waypoint');
=======
      reageerbuis.setAttribute('class', 'js--pickup clickable');
>>>>>>> Felipe
      reageerbuis.setAttribute('id', 'js--reageerbuis');
      reageerbuis.setAttribute('obj-model', 'obj: #reageerbuis-obj');
      reageerbuis.setAttribute('position', '2 2 2');
=======
      reageerbuisje.setAttribute('class', 'js--pickup clickable');
      reageerbuisje.setAttribute('id', 'js--reageerbuis');
      reageerbuisje.setAttribute('obj-model', '#reageerbuis-obj');
      reageerbuisje.setAttribute('position', '2 2 2');
>>>>>>> 49e18f5469fa8d80903bed5678c2237759d68e52

      scene.appendChild(reageerbuisje);
      addListeners();

      document.getElementById('js--hold').remove();

      hold = null;
    }
  });



  for(let i=0; i < places.length; i++){
      places[i].addEventListener("click",
      function(evt){
          let att = document.createAttribute("animation");
          att.value = "property: position; easing: linear; dur: 2000; to: " +  this.getAttribute("position").x + " 1.6 " + this.getAttribute("position").z;
          camera.setAttribute("animation", att.value);
      });
  }

  closeButton.onmouseenter = () => {
      closeButton.setAttribute("color", "yellow");
  }

  la.addEventListener("click", openLa = (event) =>{
      la.setAttribute("animation", "property: position; to: 8.5 1 -5");
      closeButton.setAttribute("color", "red");
  });

  closeButton.addEventListener("click", closeLa = (event) =>{
      la.setAttribute("animation", "property: position; to: 10 1 -5");
      closeButton.setAttribute("color", "white");
  });

}
